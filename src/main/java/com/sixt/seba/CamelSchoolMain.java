package com.sixt.seba;

import javax.jms.ConnectionFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultProducerTemplate;
import org.slf4j.*;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.camel.component.jms.JmsComponent;

public class CamelSchoolMain {

    static Logger LOGGER = LoggerFactory.getLogger(CamelSchoolMain.class);

    public static void main(String[] args) {

        String contextFilePath = "spring-context.xml";
        ClassPathXmlApplicationContext springContext = null;

        String urlList="http://www.google.com, http://www.rtvslo.si";
        /* String urlList = args.length == 1 ? args[0] : ""; */

        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://int-mq-d01.sixt.de:61676");
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(connectionFactory);
        pooledConnectionFactory.setExpiryTimeout(Long.valueOf(60000));
        pooledConnectionFactory.setIdleTimeout(30000);
        pooledConnectionFactory.setMaxConnections(10);
        pooledConnectionFactory.setMaximumActiveSessionPerConnection(500);

        try {

            springContext = new ClassPathXmlApplicationContext(contextFilePath);
            CamelContext camelContext = springContext.getBean("defCamelContext", CamelContext.class);
            camelContext.addComponent("connectionFactory",
                            org.apache.camel.component.jms.JmsComponent.jmsComponent(connectionFactory));

            LOGGER.warn("Finished loading Camel Context");

            ProducerTemplate template = new DefaultProducerTemplate(camelContext);
            template.start();
            //template.sendBody("direct:start", urlList);

        } catch (Exception e) {

            LOGGER.error("Exception occured", e);

        } finally {
            if (springContext != null) {
                springContext.close();
            }
        }
    }
}
