package com.sixt.seba;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.ConnectionFactory;

public class CamelSchoolRouteBuilder extends RouteBuilder {

    private static Logger LOG = LoggerFactory.getLogger(CamelSchoolRouteBuilder.class);

    public final static String LOG_CATEGORY = "camel.challenge";

    @Override
    public void configure() {


        LOG.warn("Configuring route definition...");

        from("connectionFactory:queue:com.sixt.workflow.eordering.FleetCrossinxTranformRouteService"
                        + "?username=wkf_einvoice" + "&password=wkf_einvoice")
                .log(org.apache.camel.LoggingLevel.WARN, LOG_CATEGORY,
                "Running measurements for input: '${in.body}'");

    }
}
